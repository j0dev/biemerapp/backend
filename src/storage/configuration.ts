import {PresentationConfig} from './Presentation'
import {Entity, Schema} from '@j0dev/entities'
import {SchemaFieldEntity} from '@j0dev/entities/dist/fields'

export class Configuration extends Entity {
    override getSchemaName(): string {
        return 'MainConfiguration'
    }

    override getSchemaVersion(): number {
        return 1
    }

    override getSchema(schema: Schema): Schema {
        super.getSchema(schema)

        schema.addField(new SchemaFieldEntity('presentation', PresentationConfig))

        return schema
    }

    presentation: PresentationConfig
}
