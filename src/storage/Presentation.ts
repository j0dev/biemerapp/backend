import {Entity, Schema} from '@j0dev/entities'
import {SchemaFieldNumber} from '@j0dev/entities/dist/fields'

export class PresentationConfig extends Entity {
    override getSchemaName(): string {
        return 'config.presentation'
    }

    override getSchemaVersion(): number {
        return 1
    }

    override getSchema(schema: Schema): Schema {
        super.getSchema(schema)

        schema.addField(new SchemaFieldNumber('defaultSlideTimeout', 60))

        return schema
    }

    defaultSlideTimeout: number
}
