.PHONY: run clean docker docker_clean

run: deps
	yarn start

deps: node_modules
	yarn install

clean:
	rm -r node_modules

docker: docker_clean
	docker build -t reg.j0dev.nl/biemerapp/backend:latest .

docker_clean:
	docker rmi reg.j0dev.nl/biemerapp/backend:latest || exit 0
